/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.AtorDAO;
import br.com.senac.modelo.Ator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrador
 */
public class CadastroAtorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mensagem = null;
        try {

            int codigo = Integer.parseInt(request.getParameter("codigo"));
            String primeiroNome = request.getParameter("primeiroNome");
            String ultimoNome = request.getParameter("ultimoNome");

           validarParametros(primeiroNome , ultimoNome);
           
            Ator ator = new Ator(codigo, primeiroNome, ultimoNome);
            
            salvar(ator);
            
            mensagem = "Salvo com sucesso.";
            
            
        } catch (NumberFormatException ex) {
            mensagem = "Somente premitido numeros no campo código";
        } catch (Exception ex) {
            mensagem = ex.getMessage();
        } finally {
            request.setAttribute("mensagem", mensagem);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("./cadastro.jsp");
        
        dispatcher.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void validarParametros(String primeiroNome, String ultimoNome) throws Exception {
        
        StringBuilder sb = new StringBuilder() ; 
        
        if(primeiroNome.trim().equals("") ){
            sb.append("Primeiro Nome.<br />") ; 
        }
        
        if(ultimoNome.trim().equals("") ){
            sb.append("Ultimo Nome.<br />") ; 
        }
        
        if(sb.toString().length() > 0 ){
            sb.insert(0, "Favor preencer os campos abaixo:<br />") ; 
            
            throw new Exception(sb.toString());
        }
        
        /*
        Favor preencer os campos abaixo:
        Primeiro Nome.
        Ultimo Nome.
        */
        
        
    }

    private void salvar(Ator ator) {
        AtorDAO dao = new AtorDAO() ; 
        
        if(ator.getCodigo() == 0 ){
            dao.salvar(ator);
        }else{
            dao.atualizar(ator);
        }
    }

}
