/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.modelo.Ator;
import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Pais;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CASA
 */
public class CidadeDAO implements DAO<Cidade> {

    @Override
    public void salvar(Cidade objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void atualizar(Cidade objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cidade> listarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Cidade buscarPorId(int id) {

        Connection connection = null;
        Cidade cidade = null;
        try {
            connection = Conexao.getConnection();
            String query = "Select * from city c inner join country co"
                    + "on c.country_id = co.country_id"
                    + "where c.city_id = ?;";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            if (rs.first()) {
                Pais pais = new Pais(rs.getInt("country_id"), rs.getString("country"));

                cidade = new Cidade(rs.getInt("city_id"), rs.getString("city"));
            }
        } catch (SQLException ex) {
            System.err.println("Erro na busca...");
        }finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

        return cidade;
    }
}
