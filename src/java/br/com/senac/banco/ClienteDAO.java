/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.modelo.Ator;
import br.com.senac.modelo.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO implements DAO<Cliente> {

    @Override
    public void salvar(Cliente cliente) {
        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            String query = "INSERT INTO customer "
                    + "(CUSTOMER_ID, FIRST_NAME , lAST_NAME, EMAIL) "
                    + "VALUES ( ?, ? , ?, ?) ; ";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, cliente.getCodigo());
            ps.setString(2, cliente.getPrimeiroNome());
            ps.setString(3, cliente.getUltimoNome());
            ps.setString(4, cliente.getEmail());

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }

    }

    @Override
    public void atualizar(Cliente cliente) {

        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            String query = "UPDATE CUSTOMER "
                    + "SET first_name = ? , last_name= ? "
                    + "SET email = ?"
                    + "WHERE actor_id = ? ; ";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, cliente.getPrimeiroNome());
            ps.setString(2, cliente.getUltimoNome());
            ps.setString(3, cliente.getUltimoNome());
            ps.setInt(4, cliente.getCodigo());

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }

    }

    @Override
    public void deletar(int id) {

        Connection connection = null;
        try {

            connection = Conexao.getConnection();

            String query = "DELETE FROM customer WHERE customer_id = ? ; ";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }
    }

    @Override
    public List<Cliente> listarTodos() {

        List<Cliente> lista = new ArrayList<>();

        Connection connection = null;
        try {

            connection = Conexao.getConnection();

            String query = "SELECT * FROM customer; ";

            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery(query);
            
            while(rs.next()){
                
                int codigo =  rs.getInt("customer_id") ; 
                String primeiroNome = rs.getString("first_name") ; 
                String ultimoNome = rs.getString("last_name") ; 
                String email = rs.getString("email") ; 
                
                Cliente cliente = new Cliente(codigo, primeiroNome, ultimoNome);
                
                lista.add(cliente);
            }
            

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }

        return lista;

    }

}

