
<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<jsp:include page="../header.jsp" />

<%
    CidadeDAO cidadeDAO = new CidadeDAO();

    List<Cidade> listaCidade = cidadeDAO.listarTodos();

%>


<div class="container">
    <fieldset>
        <legend>Cadastro de Clientes</legend>
        <form class="form-horizontal" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados Pessoais</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="codigo" name="codigo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="primeiroNome">Primeiro nome:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="primeiroNome" placeholder="Entre com o primeiro nome" name="primeiroNome">
                        </div>
                        <label class="control-label col-sm-2" for="ultimoNome">Ultimo nome:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="ultimoNome" placeholder="Entre com o Ultimo nome" name="ultimoNome">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">E-mail:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="email" placeholder="Entre com o e-mail" name="email">
                        </div>
                        <label class="control-label col-sm-2" for="telefone">Telefone:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="telefone" placeholder="Entre com o telefone" name="telefone">
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Endere�o</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="cep">CEP:</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" id="cep" name="email">
                        </div>
                        <label class="control-label col-sm-1" for="telefone">Endereco:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="telefone" placeholder="Entre com o telefone" name="telefone">
                        </div>
                        <label class="control-label col-sm-1" for="distrito">Distrito:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="endereco" placeholder="Entre com o endereco" name="endereco">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Complemento:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="email" placeholder="Entre com o e-mail" name="email">
                        </div>
                        <label class="control-label col-sm-1" for="telefone">Cidade:</label>
                        <div class="col-sm-1">
                            
                            <div style="overflow: auto; width: 110px; height: 200px">
                                
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Selecione
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <% for (Cidade c : listaCidade) {%>

                                    <li><a href="#"><%= c.getNome()%> </a></li>

                                    <% }%>
                                    

                                </ul>
                            </div>
                                    </div>
                        </div>
                        <label class="control-label col-sm-1" for="telefone">Pa�s:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="telefone" placeholder="Entre com o telefone" name="telefone">
                        </div>
                    </div>


                </div>

            </div>


            <div class="form-group">
                <div class="col-sm-offset-10 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12"> 
                            <input type="submit"   class="btn btn-primary col-" value="Salvar" />
                            <span style="margin-left: 12px" />
                            <input type="reset"    class="btn btn-danger" value="Cancelar" />
                        </div>
                    </div>



                </div>
            </div>
        </form> 

    </fieldset>


</div>



<jsp:include page="../footer.jsp" />